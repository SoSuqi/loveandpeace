import './index.scss'

interface IOptions {
  el: HTMLElement;
  total: number;
  pagesize: number;
  current?: number;
  onChange?: (current: number) => void;
}

class Pagination {
  el: IOptions['el']
  total: IOptions['total'] // 总条数
  pagesize: IOptions['pagesize'] // 每页条数
  totalPages: number // 总页数
  current: number // 当前第几页
  pages!: HTMLDivElement
  prev!: HTMLButtonElement
  next!: HTMLButtonElement
  onChange: IOptions['onChange']
  constructor ({ el, total, pagesize, current, onChange }: IOptions) {
    this.el = el
    this.total = total
    this.pagesize = pagesize
    this.totalPages = Math.ceil(total / pagesize)
    this.current = current || 1
    onChange && (this.onChange = onChange)
    this.init()
    this.renderPages()
  }
  init () {
    this.el.classList.add('pagination-wrap')
    this.el.innerHTML = `
      <button class="prev">上一页</button>
      <div class="page"></div>
      <button class="next">下一页</button>
    `
    this.bindEvent()
  }
  renderPages () {
    let str = ''
    for (let i = 1; i <= this.totalPages; i ++) {
      if (this.current === i) {
        str += `<button class="active" data-index="${i}">${i}</button>`
      } else {
        str += `<button data-index="${i}">${i}</button>`
      }
    }
    this.pages.innerHTML = str
    if (this.current === this.totalPages) this.next.disabled = true
    if (this.current === 1) this.prev.disabled = true
  }
  bindEvent () {
    this.prev = this.el.querySelector('.prev')!
    this.next = this.el.querySelector('.next')!
    this.pages = this.el.querySelector('.page')!
    this.pages.addEventListener('click', (e) => {
      const target = (e.target as HTMLButtonElement)
      if (target.nodeName === 'BUTTON') {
        const page = Number(target.getAttribute('data-index'))
        this.changeCurrent(page)
      }
    })
    this.prev?.addEventListener('click', () => {
      this.changeCurrent(this.current - 1)
    })
    this.next?.addEventListener('click', () => {
      this.changeCurrent(this.current + 1)
    })
  }
  changeCurrent (num: number) {
    this.current = num
    this.prev.disabled = false
    this.next.disabled = false
    if (this.current === this.totalPages) this.next.disabled = true
    if (this.current === 1) this.prev.disabled = true
    this.pages.querySelector('.active')?.classList.remove('active')
    this.pages.children[this.current - 1].classList.add('active')
    // 通知实例化对象分页改变了
    this.onChange && this.onChange(this.current)
  }
  changeTotal (total: number) {
    if (this.total !== total) {
      this.total = total
      this.totalPages = Math.ceil(total / this.pagesize)
      this.renderPages()
    }
  }
}

export default Pagination