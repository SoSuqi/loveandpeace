import './swiper.scss'

interface IOptions {
  el: HTMLElement;
  pagination?: HTMLElement;
  prev?: HTMLElement;
  next?: HTMLElement;
  autoplay?: { delay: number } | boolean
}

class Swiper {
  el: IOptions['el']
  wrapper: HTMLElement
  slides: Element[]
  pagination?: HTMLElement;
  prev?: HTMLElement;
  next?: HTMLElement;
  autoplay: IOptions['autoplay']
  activeIndex: number = 0; // 当前展示的元素的下标
  constructor ({ el, pagination, prev, next, autoplay }: IOptions) {
    this.el = el
    this.wrapper = this.el.querySelector('.swiper-wrapper')!
    this.slides = [...this.wrapper.children]
    this.pagination = pagination
    this.prev = prev
    this.next = next
    if (autoplay) {
      if (typeof autoplay === 'boolean') {
        this.autoplay = { delay: 1000 }
      } else {
        this.autoplay = autoplay
      }
    }
    this.init()
  }
  init () {
    // 动态计算wrapper的宽度
    this.wrapper.style.width = `${this.slides.length * 100}%`
    this.wrapper.style.transform = `translateX(${-this.activeIndex * (100 / this.slides.length)}%)`
    this.pagination && this.renderPagination()
    this.bindEvent()
    if (this.autoplay) {
      setInterval(() => {
        this.change(this.activeIndex + 1)
      }, (this.autoplay as { delay: number } ).delay)
    }
  }
  // 渲染分页器
  renderPagination () {
    let str = ''
    for (let i = 0; i < this.slides.length; i ++) {
      str += this.activeIndex === i ? `<span class="swiper-active" data-index="${i}"></span>` : `<span data-index="${i}"></span>`
    }
    this.pagination!.innerHTML = str
  }
  // 绑定事件
  bindEvent () {
    this.pagination?.addEventListener('click', (e) => {
      const target = e.target as HTMLElement
      if (target.nodeName === 'SPAN') {
        const index = Number(target.getAttribute('data-index'))
        this.change(index)
      }
    })
    this.prev?.addEventListener('click', () => {
      this.change(this.activeIndex - 1)
    })
    this.next?.addEventListener('click', () => {
      this.change(this.activeIndex + 1)
    })
  }
  // 切换图片
  change (index: number) {
    if (index < 0) index = this.slides.length - 1
    if (index > this.slides.length - 1) index = 0
    this.activeIndex = index
    this.wrapper.style.transform = `translateX(${-this.activeIndex * (100 / this.slides.length)}%)`
    // 改变分页器高亮
    this.pagination?.querySelector('.swiper-active')?.classList.remove('swiper-active')
    this.pagination?.children[index].classList.add('swiper-active')
  }
}

export default Swiper