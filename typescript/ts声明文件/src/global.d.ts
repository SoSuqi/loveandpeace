
// 全局的声明文件

// 扩展 Window 接口
interface Window {
  $aa: number;
  $tip(text: string): void;
}

// declare 给全局变量定义类型
declare var MQQ_version: string
declare let version1: string
declare const MQQ_VERSION: string
declare function getVersion (): string
declare function setVersion (version: string): void

// 定义第三方包的类型
declare module 'jquery' {
  declare function ajax(a: number[]): string
  declare const aaa: string
  declare const bbb: boolean
}


