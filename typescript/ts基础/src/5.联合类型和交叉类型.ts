{
  // 定义类型别名

  // 联合类型
  type ITest = number | string | boolean

  function fn(a: ITest) {
    // 联合类型的变量只能使用所有类型都有的方法和属性
    // a.split('') // 报错

    if (typeof a === 'string') {
      // 类型保护： 确定此判断中 a 是字符串类型
      console.log(a.indexOf)
    }
  }
  fn('abc')


  // 字面量类型
  type IWord = '张' | '李' | '王'

  let a: IWord = '李'
  function setSex(sex: 0 | 1) {
    console.log('性别', sex)
  }


  // 交叉类型
  // type I = number & string // never

  type IParams1 = { name: string; age: number }
  // 合并两个对象类型
  type IPerson = IParams1 & {
    hobby: string;
    sex: 0 | 1
  }
  

  let obj: IPerson = {
    name: 'aaaa',
    hobby: 'aaaaaaa',
    sex: 1,
    age: 100
  }


}