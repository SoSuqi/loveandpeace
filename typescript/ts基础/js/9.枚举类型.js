"use strict";
{
    // // 枚举类型定义常量
    // enum KeyCode {
    //   left = 37,
    //   up = 38,
    //   right = 39,
    //   down = 40
    // }
    // let box1: HTMLDivElement = document.querySelector('.box1')!
    // let x = 0;
    // let y = 0;
    // document.addEventListener('keydown', (e) => {
    //   const { left, top } = box1.getBoundingClientRect()
    //   if (e.keyCode === KeyCode.left) {
    //     x -= 10
    //   } else if (e.keyCode === KeyCode.up) {
    //     y -= 10
    //   } else if (e.keyCode === KeyCode.right) {
    //     x += 10
    //   } else if (e.keyCode === KeyCode.down) {
    //     y += 10
    //   }
    //   box1.style.transform = `translate(${x}px, ${y}px)`
    // })
    let ErrorCode;
    (function (ErrorCode) {
        ErrorCode[ErrorCode["Success"] = 200] = "Success";
        ErrorCode[ErrorCode["NoAuth"] = 401] = "NoAuth";
        ErrorCode[ErrorCode["Forbidden"] = 403] = "Forbidden";
        ErrorCode[ErrorCode["QueryError"] = 10086] = "QueryError";
        ErrorCode[ErrorCode["NoVip"] = 10010] = "NoVip";
        ErrorCode[ErrorCode["SpecialUser"] = 95588] = "SpecialUser";
    })(ErrorCode || (ErrorCode = {}));
    const res = { code: 200 }; // 后端返回值
    if (res.code === ErrorCode.Success) { // 成功
    }
    else if (res.code === ErrorCode.NoAuth) { // 没有登陆失效
    }
    else if (res.code === ErrorCode.Forbidden) { // 没有权限
    }
    else if (res.code === ErrorCode.QueryError) { // 参数错误
    }
    else if (res.code === ErrorCode.NoVip) { // 不是会员没有，没有资格
    }
    else if (res.code === ErrorCode.SpecialUser) { // 不是内测用户
    }
    let Direction;
    (function (Direction) {
        Direction["Up"] = "Up";
        Direction["Down"] = "Down";
        Direction["Left"] = "Left";
        Direction["Right"] = "Right";
    })(Direction || (Direction = {}));
    console.log(Direction);
}
