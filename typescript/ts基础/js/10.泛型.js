"use strict";
{
    function flat(array) {
        return array.reduce((prev, val) => {
            return prev.concat(Array.isArray(val) ? flat(val) : [val]);
        }, []);
        // while (array.some(v => Array.isArray(v))) {
        //   array = [...array]
        // }
        // return array
    }
    let arr = [[1, [2, 3, 4], 5, 6], [7, [[8, 9], 10]]];
    console.log(flat(arr));
    let arr1 = [[['a', ['b', ['c']]], 'd'], ['e', 'f', 'g'], 'h'];
    console.log(flat(arr1));
    // 泛型类
    // class Util<T, K> {
    //   name: K
    //   age: T
    //   constructor (name: K, age: T) {
    //     this.name = name
    //     this.age = age
    //   }
    //   flat<V>(array: V[][]): K {
    //     let res: V[] = []
    //     for (let i = 0; i < array.length; i ++) {
    //       res.push(...array[i])
    //     }
    //     console.log(res)
    //     return this.name
    //   }
    // }
    // let util = new Util<number, string>('工具', 100)
    // util.flat<string>(arr1)
    // // 泛型接口 
    // interface IRes<T> {
    //   code: number;
    //   msg: string;
    //   data: T
    // }
    // let res1: IRes<{ name: string; age: number; }> = {
    //   code: 200,
    //   msg: '成功',
    //   data: {
    //     name: '阿明',
    //     age: 100
    //   }
    // }
    // let res2: IRes<{ token: string; }> = {
    //   code: 200,
    //   msg: '登陆成功',
    //   data: {
    //     token: 'token111111'
    //   }
    // }
}
