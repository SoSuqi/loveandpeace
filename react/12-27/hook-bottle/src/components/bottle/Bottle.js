import React from 'react'
import classNames from 'classnames'
import style from './index.module.scss' // 使用 css-module 让组件内样式不影响全局样式

const Bottle = (props) => {
  return (
    <div className={classNames(style.bottle, { [style.active]: props.active })}>
      <i
        className={classNames(style.icon, 'iconfont', props.active ? 'icon-duigou'  : 'icon-jiajianzujianjiahao')}
        onClick={() => props.change(props.title)}
      ></i>
      <img src={props.src} alt="" />
      <h3>{props.title}</h3>
      <p>{props.price}</p>
      <div className={style.btn}>add to cart</div>
    </div>
  )
}

export default Bottle
