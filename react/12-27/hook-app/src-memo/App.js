import { useState, useCallback } from 'react'
import './App.css'
import Child from './components/Child'

function App() {
  const [title, setTitle] = useState('标题')
  const [obj, setObj] = useState({
    name: '小明',
    info: {
      age: 100,
      num: 0
    }
  })

  const addAge = useCallback(() => {
    let obj1 = JSON.parse(JSON.stringify(obj))
    obj1.info.age++
    setObj(obj1)
  }, [obj])
  const addNum = useCallback(() => {
    setObj({...obj})
    // console.log('addNum执行了')
  }, [obj])

  return (
    <div className="App">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={addAge}>addAge</button>{obj.info.age}
      </div>
      <div>
        <button onClick={addNum}>addNum</button>{obj.info.num}
      </div>
      <hr />
      <Child obj={obj} />
    </div>
  );
}

export default App;
