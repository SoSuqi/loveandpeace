import { useState, useCallback } from 'react'
import './App.css'
import Child from './components/Child'

function App() {
  const [title, setTitle] = useState('标题')
  const [num, setNum] = useState(0)

  // 父组件给子组件传函数时，使用useCallback和memo高阶组件配合使用可以减少子组件不必要的更新，优化子组件性能
  const add = useCallback(() => {
    setNum(num + 1)
  }, [num])

  return (
    <div className="App">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={add}>+</button>
        {num}
      </div>
      <hr />
      <Child num={num} add={add} />
    </div>
  );
}

export default App;
