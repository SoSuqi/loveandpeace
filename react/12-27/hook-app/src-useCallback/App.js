import { useMemo, useState, useCallback } from 'react'
import './App.css'

const arr = []

function App() {
  const [title, setTitle] = useState('标题')
  const [num, setNum] = useState(0)

  // 返回一个缓存的函数，依赖项改变重新创建函数，没改变从缓存中读取函数
  // const add = useCallback(() => {
  //   console.log(num)
  //   setNum(num + 1)
  // }, [num])

  // 可以通过useMemo实现useCallback的功能
  const add = useMemo(() => {
    return () => {
      console.log(num)
      setNum(num + 1)
    }
  }, [num])

  if (arr.indexOf(add) === -1) {
    arr.push(add)
  }
  console.log(arr)

  return (
    <div className="App">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={add}>+</button>
        {num}
      </div>
    </div>
  );
}

export default App;
