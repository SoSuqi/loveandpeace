import React, { useState, useReducer, useEffect, useLayoutEffect } from 'react'

const init = {
  name: '小明',
  age: 100,
  info: {
    sex: '男',
    hobby: [1, 2, 3, 4]
  }
}

// 修改和返回新的state
const reducer = (state, action) => {
  // 根据type判断本次要修改哪些数据
  if (action.type === 'name') {
    return {...state, name: action.payload}
  } else if (action.type === 'name-age') {
    return {...state, name: action.payload.name, age: action.payload.age}
  }
  return state
}

const App = () => {

  const [state, dispatch] = useReducer(reducer, init)

  return (
    <div>
      <h1>App</h1>
      <div> 姓名： {state.name} </div>
      <div>年龄： {state.age} </div>
      <div>性别： {state.info.sex} </div>
      <div>爱好： {state.info.hobby} </div>
      <button onClick={() => dispatch({ type: 'name', payload: '小刚' })}>修改姓名</button>
      <button onClick={() => dispatch({ type: 'name-age', payload: { name: '小红', age: 200 } })}>修改年龄和姓名</button>
      <button>修改信息</button>
    </div>
  )
}

export default App