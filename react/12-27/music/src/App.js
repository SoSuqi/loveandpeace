import React, { useRef, useState, useEffect } from 'react'
import './App.scss'
import axios from 'axios'
import Keyboard from './components/keyboard/Keyboard'
import Controller from './components/controller/Controller'

const App = () => {
  const [list, setList] = useState([]) // 所有数据
  const [curMusic, setCurMusic] = useState({})  // 当前按下的按钮
  const [flag, setFlag] = useState(true) // power
  const [bank, setBank] = useState(false) // bank
  const url = bank ? curMusic.bankUrl : curMusic.url
  const audio = useRef(null)

  const loadAudio = (data) => {
    data.forEach(item => {
      const audio = new Audio()
      audio.src = item
      audio.addEventListener('canplay', () => {
        console.log('加载成功', item)
      })
    })
  }

  const getList = async () => {
    const res = await axios.get('/list')
    setList(res.data)
    const list = res.data.map(item => [item.url, item.bankUrl]).flat()
    loadAudio(list)
  }

  useEffect(() => {
    getList()
  }, [])

  const changeVolume = (volume) => {
    audio.current.volume = volume
  }
  const changeBank = () => {
    setCurMusic({})
    setBank(!bank)
  }
  const changeFlag = () => {
    if (flag) {
      setFlag(false)
      setCurMusic({})
    } else {
      setFlag(true)
    }
  }
  const changeMusic = (music) => {
    // 按钮开启允许播放音乐
    if (flag) {
      setCurMusic(music)
      audio.current.load()
    }
  }

  return  (
    <div className='wrap'>
      <Keyboard list={list} flag={flag} changeMusic={changeMusic} />
      <Controller
        curMusic={curMusic}
        flag={flag}
        bank={bank}
        changeBank={changeBank}
        changeFlag={changeFlag}
        changeVolume={changeVolume}
      />
      <audio ref={audio} src={url} autoPlay></audio>
    </div>
  )
}

export default App