import { useState, useEffect, useMemo, useCallback, useRef, useLayoutEffect, useReducer } from 'react'
import Child from './components/Child'


const Index = () => {
  const [count, setCount] = useState(0)
  const [num, setNum] = useState(0)
  const add = () => {
    setNum(n => n + 1)
    setNum(n => n + 1)
    setNum(n => n + 1)
  }

  // 处理副作用，可以实现类似class组件的生命周期功能
  useEffect(() => {
    const timer = setInterval(() => {
      setCount(c => {
        // console.log(c)
        return c + 1
      })
    }, 1000)
    // document.addEventListener('click', () => {
    //   setCount(c => c + 1)
    // })

    return () => {
      // 组件销毁执行，类似 componentWillUnmount
      clearInterval(timer)
    }
  }, []) // 传入空数组只执行，类似 componentDidMount

  useEffect(() => {
    // 第二个参数不传类似 componentDidUpdate
    // console.log(count)
  })

  const getlist = async () => {
    // const res = await axios.get('xxxxx')
    // console.log(res)
  }

  useEffect(() => {
    // useEffect 函数不能写async，因为 useEffect 需要返回一个函数当组件销毁时执行，async 函数返回一个promise实例
    console.log(num, count)
    getlist()
  }, [num])


  // 返回一个缓存的值，当依赖项改变时重新执行
  const total = useMemo(() => {
    return num.toFixed(2)
  }, [num])

  // 返回一个缓存的函数，当依赖项改变时重新创建函数
  const fn = useCallback(() => {
    console.log('返回一个缓存的函数', num)
  }, [num])

  // useRef
  // 1. 获取dom
  // 2. 存变量，变量改变不会触发组件更新，组件更新时始终指向同一个地址，类似class组件的实例属性
  // 3. 可以获取class子组件的实例对象，调用子组件的方法
  // 4. 可以获取函数子组件内部的方法和数据，

  // 数据更新后，页面渲染前执行
  useLayoutEffect(() => {})

  // const [state, dispatch] = useReducer(reducer)

  return (
    <div>
      <div>定时器：{count}</div>
      <div>
        <button onClick={add}>+</button>{num}
      </div>
      <hr />
      <Child num={num} fn={fn} />
    </div>
  )
}

export default Index