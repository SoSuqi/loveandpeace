import { createStore } from 'redux'

const init = {
  num: 0  
}

// 返回和修改数据
const reducer = (state = init, action) => {
  // action：组件中通过dispatch发送的action对象
  console.log(action)
  if (action.type === 'ADD') {
    return {...state, num: state.num + action.payload}
  }
  return state
}

const store = createStore(reducer)

export default store
