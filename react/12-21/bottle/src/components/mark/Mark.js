import React, { Component } from 'react'
import classNames from 'classnames'
import style from './index.module.scss'

// console.log(style)

class Mark extends Component {

  state = {
    out: false
  }
  
  close = () => {
    this.setState({
      out: true
    })
  }
  end = () => {
    if (this.state.out) {
      this.props.close()
    }
  }
  render() {
    const { out } = this.state
    return (
      <div className={style.mark}>
        {this.props.list.map(item =>
          <div key={item.title} className={classNames(style.mark_item, { [style.out]: out })} onAnimationEnd={this.end}>
            <img src={item.src} alt="" />
            <h3>{item.title}</h3>
            <p>{item.alcohol}</p>
            <p>{item.price}</p>
            <p>{item.region}</p>
            <p>{item.varietal}</p>
            <p>{item.year}</p>
            <div className={style.btn}><i className='iconfont icon-gouwucheman'></i>add to cart</div>
          </div>
        )}
        {!out && <div className={classNames("iconfont icon-icon_close", style.close)} onClick={this.close}></div>}
      </div>
    )
  }
}

export default Mark