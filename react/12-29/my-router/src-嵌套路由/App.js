import React from 'react'
import './App.css'
import {
  Switch, // 只渲染匹配到的第一个路由
  Route, // 配置路由视图
  Redirect, // 重定向
} from 'react-router-dom'
import Home from './pages/home/Home'
import City from './pages/city/City'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import NotFound from './pages/404'

import Movie from './pages/home/movie/Movie'
import Cinema from './pages/home/cinema/Cinema'
import News from './pages/home/news/News'
import Mine from './pages/home/mine/Mine'

import Hot from './pages/home/movie/hot/Hot'
import Coming from './pages/home/movie/coming/Coming'

const App = () => {
  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/city" component={City} />
      <Route exact path="/detail" component={Detail} />
      <Route exact path="/404" component={NotFound} />
      <Route path="/" render={(routeInfo) => (
        <Home {...routeInfo}>
          <Switch>
            <Route path="/movie" render={(routeInfo) => (
              <Movie {...routeInfo}>
                <Switch>
                  <Route exact path="/movie/hot" component={Hot} />
                  <Route exact path="/movie/coming" component={Coming} />
                  <Redirect exact from="/movie" to="/movie/hot" />
                  <Redirect to="/404" />
                </Switch>
              </Movie>
            )} />
            <Route exact path="/cinema" component={Cinema} />
            <Route exact path="/news" component={News} />
            <Route exact path="/mine" component={Mine} />
            <Redirect exact from='/' to="/movie" />
            <Redirect to="/404" />
          </Switch>
        </Home>
      )} />
    </Switch>
  )
}

export default App




// const App = () => {
//   return (
//     <div>
//       <Switch>
//         {/* 传给 Route 组件的 component 属性的组件，会在 Route 组件内调用并且把当前路由信息传到该组件的 props 中 */}
//         <Route exact path="/login" component={Login} />
//         <Route exact path="/city" render={(routeInfo) => {
//           return <City title="abc" {...routeInfo} />
//         }} />
//         <Route exact path="/detail" component={Detail} />
//         <Route exact path="/404" component={NotFound} />
//         <Route path="/">
//           <Home>
//             <Switch>
//               <Route path="/movie">
//                 <Movie>
//                   <Switch>
//                     <Route exact path="/movie/hot" component={Hot} />
//                     <Route exact path="/movie/coming" component={Coming} />
//                     <Redirect exact from="/movie" to="/movie/hot" />
//                     <Redirect to="/404" />
//                   </Switch>
//                 </Movie>
//               </Route>
//               <Route exact path="/cinema" component={Cinema} />
//               <Route exact path="/news" component={News} />
//               <Route exact path="/mine" component={Mine} />
//               <Redirect exact from='/' to="/movie" />
//               <Redirect to="/404" />
//             </Switch>
//           </Home>
//         </Route>
//       </Switch>
//     </div>
//   )
// }

// export default App