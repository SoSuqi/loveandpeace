import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter, // 路由根组件，history 模式， 根组件一个项目中只需要使用一次
  HashRouter // 路由根组件，hash 模式
} from 'react-router-dom'
import routes from './router/config'
import Routerview from './router/Routerview'
import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <Routerview routes={routes} />
  </HashRouter>
);

