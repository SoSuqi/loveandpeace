import React from 'react'

class App extends React.Component {
  state = {
    num: 10
  }
  add = () => {
    // 1. 把传入的对象和原本的state进行合并
    // 2. setState 是异步的
    // 3. 可以传入第二个参数，是页面更新完成后的回调函数，类似vue中的 $nextTick
    this.setState({
      num: this.state.num + 1
    }, () => {
      // console.log('页面更新完成', this.state.num)
    })
    // 获取不到最新的数据
    // console.log(this.state.num)
  }

  sub = () => {
    // 4. 同时调用多次 setState，会合并成一次更新
    // this.setState({ num: this.state.num - 1 })
    // this.setState({ num: this.state.num - 1 })
    // this.setState({ num: this.state.num - 1 })
    // this.setState({ num: this.state.num - 1 })
    // this.setState({ num: this.state.num - 1 })
    
    // 5. 第一个参数可以传入一个函数，可以获取到组件最新的数据
    // this.setState(state => {
    //   // state: 最新的数据
    //   // 需要返回一个对象，把此对象和原本的state进行合并
    //   return {
    //     num: state.num - 1
    //   }
    // })
    // this.setState(state => ({ num: state.num - 1 }))
    // this.setState(state => ({ num: state.num - 1 }))
    // this.setState(state => ({ num: state.num - 1 }))
    // this.setState(state => ({ num: state.num - 1 }))

    // 6. react18之前 setState 在原生事件和setTimeout中是同步的，在react事件和生命周期中是异步的
  }
  
  render() {
    const { num } = this.state
    console.log('执行render函数')
    return (
      <div>
        <h1>class 组件</h1>
        <div>
          <button onClick={this.sub}>-</button>
          <b>{num}</b>
          <button onClick={this.add}>+</button>
        </div>
      </div>
    )
  }

}
export default App
