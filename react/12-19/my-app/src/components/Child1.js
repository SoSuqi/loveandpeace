import React from 'react'

class Child1 extends React.Component {

  constructor(props) {
    super(props)
    // 如果定义了 constructor 必须把props传给 super
    // console.log(this.props)
  }

  render () {
    // console.log(this.props)
    // this.props 接收父组件传过来的参数
    const { num, title, changeNum, left, right, btm, children } = this.props
    return (
      <div>
        <h2>Child1 - {title}</h2>
        <button onClick={() => changeNum(-1)}>-</button>
        {num}
        <button onClick={() => changeNum(1)}>+</button>
        {left}
        {right}
        {btm}
        <div style={{ border: '5px solid red' }}>
          {children}
        </div>
      </div>
    )
  }
}

export default Child1
