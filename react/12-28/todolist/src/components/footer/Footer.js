import React, { useMemo } from 'react'
import style from './index.module.scss'
import classNames from 'classnames'

const btns = [
  {
    text: '全部',
    status: 0
  },
  {
    text: '进行中',
    status: 1
  },
  {
    text: '已完成',
    status: 2
  }
]

const Footer = ({ done, todo, status, onClear, onChange }) => {
  const showBtn = done > 0 && todo > 0

  const renderBtn = (item) => {
    if (item.status === 0 || showBtn) {
      return <button
        key={item.status}
        className={classNames({ [style.active]: status === item.status })}
        onClick={() => onChange(item.status)}
      >{item.text}</button>
    }
    return null
  }

  return (
    <div className={style.footer}>
      <div>{todo} 项未完成</div>
      <div>
        {btns.map(renderBtn)}
      </div>
      {done > 0 && <button onClick={onClear}>清除已完成</button>}
    </div>
  )
}

export default Footer