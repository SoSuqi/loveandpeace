import React, { useRef, memo } from 'react'
import style from './index.module.scss'

const Form = props => {
  const inp = useRef(null)
  const submit = () => {
    if (inp.current.value.trim()) {
      props.add(inp.current.value)
    }
    inp.current.value = ''
    inp.current.focus()
  }
  const keyDown = (e) => {
    if (e.keyCode === 13) {
      submit()
    }
  }

  return (
    <div className={style.form}>
      <h1>~ Today I need to ~</h1>
      <div className={style.formWrapper}>
        <div className={style.formInput}>
          <input ref={inp} onKeyDown={keyDown} placeholder="Add new todo..." autoFocus />
        </div>
        <button type="submit" className={style.submitBtn} onClick={submit}>
          <span>Submit</span>
        </button>
      </div>
    </div>
  )
}

export default memo(Form)