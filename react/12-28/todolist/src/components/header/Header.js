import React from 'react'
import style from './index.module.scss'

const Header = () => {
  return (
    <div className={style.header}>
      <div className={style.imgWraper}><img src="https://www.todolist.cn/img/note.75134fb0.svg" alt="Note" /></div>
      <div className={style.title}>To-Do List</div>
    </div>
  )
}

export default Header