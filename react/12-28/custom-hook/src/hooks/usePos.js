import { useState, useEffect, useCallback } from 'react'

// 自定义hook：抽取组件中的公用逻辑
// 1. 名字必须以 use 开头
// 2. 自定义hook中可以使用其他hook，普通函数中无法使用hook

const usePos = (num) => {
  const [pos, setPos] = useState({x: 0, y: 0})

  const mousemove = useCallback(e => {
    setPos({ x: e.pageX, y: e.pageY })
  }, [])

  useEffect(() => {
    document.addEventListener('mousemove', mousemove)
    return () => {
      document.removeEventListener('mousemove', mousemove)
    }
  }, [])

  useEffect(() => {
    if (num && num > 10) {
      document.removeEventListener('mousemove', mousemove)
    }
  }, [num])

  return pos
}

export default usePos