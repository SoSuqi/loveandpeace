import { createStore, applyMiddleware } from 'redux'
// 发起 dispatch 时自动打印仓库数据
import logger from 'redux-logger'
// redux-thunk: redux 处理异步的中间件
// 让 dispatch 可以接收函数作为参数
import thunk from 'redux-thunk' 
import {
  SET_LIST,
  SET_NAME,
  SET_AGE,
  SET_SEX,
  SET_GOODS
} from './actions'

const init = {
  list: [],
  name: '',
  age: 100,
  sex: '男',
  goods: []
}

const reducer = (state = init, action) => {
  if (action.type === SET_LIST) {
    return { ...state, list: action.payload }
  } else if (action.type === SET_NAME) {
    return { ...state, name: action.payload }
  } else if (action.type === SET_AGE) {
    return { ...state, age: action.payload }
  } else if (action.type === SET_SEX) {
    return { ...state, sex: action.payload }
  } else if (action.type === SET_GOODS) {
    return { ...state, goods: action.payload }
  }
  return state
}


const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store