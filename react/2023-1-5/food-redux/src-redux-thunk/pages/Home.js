import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setListAction } from '../store/actions'


const Home = () => {
  const list = useSelector(s => s.list)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setListAction)
  }, [])

  return (
    <div>
      <ul>
        {list.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width="300" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home