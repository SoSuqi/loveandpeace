const init = {
  name: '',
  age: 100,
  sex: '',
  email: ''
}

const user = (state = init, action) => {

  if (action.type === 'ADD_AGE') {
    return {
      ...state,
      age: state.age + 1
    }
  } else if (action.type === 'SUB_AGE') {
    return {
      ...state,
      age: state.age + 1
    }
  }
  return state
}

export default user