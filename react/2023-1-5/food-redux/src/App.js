import {
  Switch,
  Route,
  NavLink
} from 'react-router-dom'
import Home from './pages/Home'
import User from './pages/User'
import NotFound from './pages/404'

const App = () => {
  return (
    <div className="App">
      <main>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/user" component={User} />
          <Route component={NotFound} />
        </Switch>
      </main>
      <footer>
        <NavLink exact to="/">首页</NavLink>
        <NavLink exact to="/user">用户</NavLink>
      </footer>
    </div>
  )
}

export default App