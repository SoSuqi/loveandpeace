import axios from 'axios'

export const getBanner = () => {
  return axios.get('https://zyxcl-music-api.vercel.app/banner')
} 