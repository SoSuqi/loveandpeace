import React from 'react'
import { useSelector, IndexModelState, useDispatch } from 'umi'
import { Space, Table, Tag, Pagination, Select } from 'antd';
import type { ColumnsType } from 'antd/es/table';

const Option = Select.Option
const Child2 = () => {
  const username = useSelector((s: { user: IndexModelState }) => s.user.name)
  const dispatch = useDispatch()

  return (
    <div>
      {/* <h2>Child2 - {username}</h2>
      <input type="text" value={username} onChange={e => {
        dispatch({
          type: 'user/changeName',
          payload: e.target.value
        })
      }} /> */}
      <hr />
    </div>
  )
}

export default Child2