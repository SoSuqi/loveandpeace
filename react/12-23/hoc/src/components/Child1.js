import React, { Component } from 'react'
import axios from 'axios'
import withSize from '../hoc/withSize'

 class Child1 extends Component {

  state = {
    list: []
  }

  componentDidMount () {
    axios.get('/list').then(res => {
      this.setState({ list: res.data })
    })
  }

  renderList = () => {
    const { list } = this.state
    const { size } = this.props
    if (size === 'big') {
      return <ul>
        {list.map(item => <li key={item.id}>{item.id}</li>)}
      </ul>
    } else if (size === 'middle') {
      return <p>
        {list.map(item => <b key={item.id}>{item.id}</b>)}
      </p>
    } else {
      return JSON.stringify(list)
    }
  }

  render() {
    // console.log(this.props)
    return (
      <div className='child1'>
        <h2>Child1</h2>
        {this.renderList()}
      </div>
    )
  }
}
// 
export default withSize(Child1)