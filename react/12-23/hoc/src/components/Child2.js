import React, { Component } from 'react'
import withSize from '../hoc/withSize'

class Child2 extends Component {

  radiusEnum = {
    big: 0,
    middle: '20px',
    small: '50%'
  }

  render() {
    const { size } = this.props
    return (
      <div className='child2'>
        <h2>Child2</h2>
        <div style={{ borderRadius: this.radiusEnum[size] }} className="box"></div>
      </div>
    )
  }
}

// 高阶组件：参数为组件并且返回一个新组件的函数
// 作用：抽取公共逻辑，给组件添加额外的功能
export default withSize(Child2)
