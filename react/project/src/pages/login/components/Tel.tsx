import { useState } from 'react'
import { LockOutlined, PhoneOutlined } from '@ant-design/icons';
import { Button, Form, Input, Row, Col } from 'antd';

const Account = () => {
  const [num, setNum] = useState(0)
  const [form] = Form.useForm()
  const onFinish = (values: any) => {
    console.log('校验通过 ', values);
    // 表单校验通过，调用登陆接口
  }
  // 调用验证码接口
  const getVerifyCodeApi = () => {
  }
  const getVerifyCode = () => {
    // 校验手机号是否正确
    form.validateFields(['tel']).then(res => {
      // 按钮显示倒计时
      setNum(10)
      const timer = setInterval(() => {
        setNum(n => {
          if (n - 1 === 0) {
            clearInterval(timer)
          } 
          return n - 1
        })
      }, 1000)
      getVerifyCodeApi()
    })
  }

  return <Form onFinish={onFinish} form={form}>
    <Form.Item
      name="tel"
      rules={[
        { required: true, message: '请输入手机号!' },
        { pattern: /^1[3-9]\d{9}$/, message: '手机号格式错误' }
      ]}
    >
      <Input prefix={<PhoneOutlined />} placeholder="手机号" />
    </Form.Item>
    <Form.Item
      name="code"
      rules={[{ required: true, message: '请输入验证码!' }]}
    >
      <Row gutter={16}>
        <Col span={14}><Input prefix={<LockOutlined />} placeholder="验证码"/></Col>
        <Col span={10}>
          <Button block onClick={getVerifyCode} disabled={num > 0}>
            {num > 0 ? `${num}s后重新获取` :'获取验证码'}
          </Button>
        </Col>
      </Row>
    </Form.Item>
    <Form.Item>
      <Button type="primary" block htmlType='submit'>登陆</Button>
    </Form.Item>
  </Form>
}

export default Account