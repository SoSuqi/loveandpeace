import { getArticleList } from '@/services'
import { Tag, Button } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import moment from 'moment'
import Table from '@/components/table'
import { Link } from 'umi'

const List = () => {
  const columns: ColumnsType<ArticleItem> = [
    {
      title: '文章标题',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: '文章标签',
      dataIndex: 'keyCode',
      key: 'keyCode',
      render: (keyCode: ArticleKeyCode[]) => {
        return keyCode.map(item => <Tag key={item.keyCode} color={item.color}>{item.title}</Tag>)
      }
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: _ => moment(_).format('YYYY-MM-DD kk:mm:ss')
    },
    {
      title: '文章简介',
      dataIndex: 'desc',
      key: 'desc',
    },
    {
      title: '发布者',
      dataIndex: 'author',
      key: 'author',
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return <Button type="primary"><Link to={`/detail/${record.id}`}>查看</Link></Button>
      }
    },
  ];
  return (
    <div>
      <Table
        columns={columns}
        getListApi={getArticleList}
        rowKey="id"
        query={{
          page: 1,
          pagesize: 5
        }}
        pageSizeOptions={[5, 10, 20]}
      />
    </div>
  )
}

export default List