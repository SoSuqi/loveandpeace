import React, { useEffect, useState } from 'react'
import { getArticleTypeList } from '@/services'
import { Column } from '@ant-design/plots';

const Dashboard = () => {
  const [data, setData] = useState<ArticleTypeItem[]>([])
  const getArticleTypeData = async () => {
    const res = await getArticleTypeList()
    setData(res.values.list)
  }
  useEffect(() => {
    getArticleTypeData()
  }, [])

  const config = {
    data,
    xField: 'title',
    yField: 'count',
    seriesField: '',
    color: ({ title }: { title: string }) => {
      return data.find(v => v.title === title)?.color
    },
    label: {
      content: (originData: ArticleTypeItem) => {
        return `${originData.title}/${originData.count}`
      },
      offset: 10,
    },
    legend: {
      layout: 'horizontal',
      position: 'right'
    },
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
  };
  return (
    <div>
      <Column {...config} />
    </div>
  )
}

export default Dashboard