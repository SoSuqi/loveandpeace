import React, { useMemo } from 'react'
import {
  UserOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined
} from '@ant-design/icons';
import {
  Layout,
  Breadcrumb,
  Avatar,
  Image,
  Dropdown,
  message
} from 'antd';
import type { MenuProps } from 'antd';
import style from '../index.less'
import { useSelector, UserModelState, history } from 'umi'
import { logout } from '@/services'

const { Header } = Layout;

interface IProps {
  collapsed: boolean;
  setCollapsed: (collapsed: boolean) => void
}

const items: MenuProps['items'] = [
  {
    key: 'userinfo',
    label: '个人中心',
  },
  {
    key: 'logout',
    label: '退出登陆',
    danger: true
  }
];

const LayoutHeader: React.FC<IProps> = (props) => {
  const userInfo = useSelector((state: { user: UserModelState }) => state.user.userInfo)

  const onClick: MenuProps['onClick'] = async ({ key }) => {
    if (key === 'logout') {
      try {
        const res = await logout()
        if (res.code === 200) {
          localStorage.removeItem('token')
          history.push('/login')
          message.success('退出登陆')
        } else {
          message.error(res.msg)
        }
      } catch (e) {
        message.error('网络错误，请稍后重试！')
      }
    }
  }

  const avatar = useMemo(() => {
    let avatar = userInfo.avatar || ''
    return avatar.replace('localhost', '121.89.213.194')
  }, [userInfo.avatar])

  return (
    <Header className={style.header}>
      <div className={style.header_left}>
        <div className={style.trigger} onClick={() => props.setCollapsed(!props.collapsed)} >
          {props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        </div>
        <Breadcrumb>
          <Breadcrumb.Item>首页</Breadcrumb.Item>
          <Breadcrumb.Item>添加文章</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className={style.header_right}>
        {avatar ?
          <Avatar src={<Image src={avatar} />} />
        :
          <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
        }  
        <Dropdown menu={{ items, onClick  }}>
          <div style={{ marginLeft: 10 }}>{userInfo.account}</div>
        </Dropdown>
      </div>
    </Header>
  )
}

export default LayoutHeader