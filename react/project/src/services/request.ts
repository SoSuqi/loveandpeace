import axios from 'axios'
import { history } from 'umi'
import { message } from 'antd'

// 二次封装axios
// 创建axios实例对象
const instance = axios.create({
  baseURL: 'http://121.89.213.194:5000',
  timeout: 5000
})

// 添加请求拦截器，在请求之前统一给接口添加公共参数，例如token
instance.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  const token = localStorage.getItem('token')
  if (token) {
    (config.headers as any).authorization = token
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器，请求响应成功后处理公共错误，例如：401、403
instance.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  // console.log('响应成功拦截', response)
  return response.data
}, function (error) {
  // console.log('响应失败拦截', error)
  if (error.response.status === 401) {
    history.push('/login')
    localStorage.removeItem('token')
    message.error('登陆信息失效，请重新登陆！')
  } else if (error.response.status === 403) {
    history.push('/403')
    message.error('没有权限访问此页面！')
  }
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default instance
