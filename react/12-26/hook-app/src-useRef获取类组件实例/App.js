import React, { useEffect, useRef } from 'react'
import Child1 from './components/Child1'

// 使用 useRef 获取类组件实例对象
const App = () => {
  const child1 = useRef(null)

  useEffect(() => {
    console.log(child1.current)
  }, [])

  return (
    <div>
      <Child1 ref={child1} />
      <hr />
      <button onClick={() => {
        // 父组件调用子组件的方法
        child1.current.add()
      }}>+</button>
    </div>
  )
}

export default App