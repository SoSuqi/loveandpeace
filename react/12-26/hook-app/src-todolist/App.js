import React, { useState } from 'react'


const App = () => {
  const [list, setList] = useState([])
  const [text, setText] = useState('')
  const add = () => {
    setList([...list, { id: Date.now(), done: false, text }])
    setText('')
  }
  const remove = id => {
    setList(list.filter(v => v.id !== id))
  }
  const done = id => {
    setList(list.map(item => {
      return item.id === id ? { ...item, done: !item.done } : item
    }))
  }
  return (
    <div>
      <input type="text" value={text} onChange={e => setText(e.target.value)} />
      <button onClick={add}>添加</button>
      <ul>
        {list.map(item =>
          <li key={item.id}>
            {item.done ? <s>{item.text}</s> : item.text}
            <button onClick={() => remove(item.id)}>删除</button>
            <button onClick={() => done(item.id)}>done</button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default App