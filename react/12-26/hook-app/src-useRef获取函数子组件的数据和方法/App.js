import React, { useEffect, useRef } from 'react'
import Child1 from './components/Child1'

// 使用 useRef 获取函数子组件的数据和方法
const App = () => {
  const child1 = useRef(null)

  return (
    <div>
      <Child1 ref={child1} />
      <hr />
      <button onClick={() => {
        console.log(child1.current)
        child1.current.add()
      }}>+</button>
    </div>
  )
}

export default App