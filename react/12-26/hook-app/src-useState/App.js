import { useState } from 'react'

// hook名称必须以use开头
// hook作用: 让函数式组件可以实现classs组件的功能，使函数组件可以代替类组件
// hook使用规则：
// 1. 只在最顶层使用 Hook，不要在循环，条件或嵌套函数中调用 Hook
// 2. 只在 React 函数组件或者自定义hook中调用 Hook，不能在普通函数中使用hook


function App() {
  // 函数组件中定义数据, [数据， 修改数据的函数]
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('hook标题')
  const [flag, setFlag] = useState(false)
  const [arr, setArr] = useState([0, 1, 2, 3])
  const [obj, setObj] = useState({
    name: '小明',
    age: 20
  })

  const changeCount = (n) => {
    // 传入的参数会覆盖当前数据，异步更新
    setNum(num + n)

    // 传入函数可以通过函数的参数获取到最新的数据，返回值会覆盖当前数据，异步更新
    // setNum(a => a + n)
    // setNum(a => a + n)
    // setNum(a => a + n)
    // setNum(a => {
    //   console.log(a)
    //   return a + n
    // })
    // setNum(a => a + n)
  }

  return (
    <div className="App">
      <h1>{title}</h1>
      {flag && <input type="text" value={title} onChange={e => setTitle(e.target.value)} />}
      <button onClick={() => setFlag(!flag)}>显示隐藏input</button>
      <hr />
      <button onClick={() => changeCount(-1)}>-</button>
      {num}
      <button onClick={() => changeCount(1)}>+</button>
      <hr />
      <button onClick={() => {
        setArr([...arr, arr.length])
      }}>add</button>
      <ul>
        {arr.map(item => <li key={item}>{item}</li>)}
      </ul>
      <hr />
      <div>
        <p>
          姓名: {obj.name}
          <input type="text" value={obj.name} onChange={e => setObj({ ...obj, name: e.target.value })} />
        </p>
        <p>
          年龄: {obj.age}
          <input type="text" value={obj.age} onChange={e => setObj({ ...obj, age: e.target.value })} />
        </p>
      </div>
    </div>
  );
}

export default App;
