import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import style from './hot.module.scss'
import Film from './Film'
import movieCtx from '../../../../context/movieCtx'

const Hot = (props) => {
  // props 中包含当前路由信息
  const [list, setList] = useState([]) // 列表数据
  const [pagenum, setPagenum] = useState(1) // 当前第几页
  const [loading, setLoading] = useState(false) // 是否正在请求接口
  const [total, setTotal] = useState(0) // 总数
  const { isDown, setIsDown } = useContext(movieCtx) // 父级是否滚动到底部

  const getList = async () => {
    setIsDown(false)
    setLoading(true)
    const res = await axios.get('/hot/movie', {
      params: {
        pagesize: 10,
        pagenum
      }
    })
    if (res.data.status === 0) {
      setList([...list, ...res.data.data.films])
      setPagenum(pagenum + 1)
      setTotal(res.data.data.total)
    } else {
      alert(res.data.msg)
    }
    setLoading(false)
  }

  useEffect(() => {
    if (list.length === 0 || (isDown & !loading && list.length < total)) {
      getList()
    }
  }, [isDown, list])

  return (
    <div className={style.hot}>
      {list.map(item =>
        <Film key={item.filmId} film={item} />
      )}
      {list.length > 0 && list.length >= total && <div style={{ height: 40, lineHeight: '40px', textAlign: 'center', background: '#ccc' }}>没有更多数据了</div>}
    </div>
  )
}

export default Hot