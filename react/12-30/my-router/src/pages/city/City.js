import React, { useEffect, useState, useCallback } from 'react'
import style from './city.module.scss'
import axios from 'axios'
import Result from './components/Result'
import Floor from './components/Floor'

const City = (props) => {

  const [cities, setCities] = useState([]) // 城市列表原数据
  const [floor, setFloor] = useState([]) // 根据城市首字母排序后的数据
  const [searchText, setSearchText] = useState('') // 搜索框内容
  const [showCancel, setShowCancel] = useState(false) // 取消按钮
  const [showSearchRes, setShowSearchRes] = useState(false) // 搜索结果
  const [searchResult, setSearchResult] = useState([]) // 搜索结果
  
  // 格式化楼层数据
  const formatFloor = useCallback((cities) => {
    const floor = []
    const map = new Map()
    const hot = []
    for (let item of cities) {
      const type = item.pinyin[0].toUpperCase()
      if (item.isHot === 1) {
        hot.push(item)
      }
      if (map.get(type)) {
        map.get(type).push(item)
      } else {
        let list = [item]
        floor.push({
          type,
          list
        })
        map.set(type, list)
      }
    }
    floor.sort((a, b) => a.type.charCodeAt() - b.type.charCodeAt())
    floor.unshift({
      type: '热门城市',
      isHot: 1,
      list: hot
    })
    setFloor(floor)
  }, [])

  // 请求接口
  useEffect(() => {
    axios.get('/api/city').then(res => {
      const cities = res.data.data.cities
      setCities(cities)
      formatFloor(cities)
    })
  }, [])


  // 修改搜索内容
  const changeInp = e => {
    setSearchText(e.target.value)
    setShowSearchRes(true)
  }
  const cancelSearch = () => {
    setShowCancel(false)
    setShowSearchRes(false)
  }

  useEffect(() => {
    if (searchText === '') {
      setSearchResult([])
    } else {
      setSearchResult(cities.filter(v => v.name.includes(searchText) || v.pinyin.includes(searchText)))
    }
  }, [searchText])

  return (
    <div className={style.city}>
      <div className={style.title}>
        <div className={style.close}>x</div>
        <h3>选择城市</h3>
      </div>
      <div className={style.search}>
        <div className={style.input}>
          <svg t="1672449321795" className={style.s_icon} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2758" width="16" height="16"><path d="M938.64603 886.273219l-173.071777-173.074847c53.665247-63.987337 86.075401-146.400325 86.075401-236.446154 0-203.406666-164.895561-368.298134-368.301204-368.298134-203.409736 0-368.302227 164.892491-368.302227 368.298134 0 203.409736 164.892491 368.302227 368.302227 368.302227 89.246627 0 171.055864-31.767518 234.798631-84.579327l173.148525 173.148525c1.576915 1.576915 8.15575-2.443655 14.6957-8.979512l23.675212-23.675212C936.205445 894.428969 940.222945 887.850134 938.64603 886.273219zM483.347426 778.093381c-166.425404 0-301.338093-134.912689-301.338093-301.338093s134.912689-301.338093 301.338093-301.338093S784.685519 310.329884 784.685519 476.755288 649.773853 778.093381 483.347426 778.093381z" fill="#808488" p-id="2759"></path></svg>
          <input
            type="text"
            placeholder='请输入城市名称或者拼音'
            value={searchText}
            onChange={changeInp}
            onFocus={() => setShowCancel(true)}
          />
          {showCancel && searchText.length > 0 && <svg t="1672449406287" onClick={() => setSearchText('')} className={style.s_icon} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3805" width="16" height="16"><path d="M512 64c-247.00852 0-448 200.960516-448 448S264.960516 960 512 960c247.00852 0 448-200.960516 448-448S759.039484 64 512 64zM694.752211 649.984034c12.480043 12.54369 12.447359 32.768069-0.063647 45.248112-6.239161 6.208198-14.399785 9.34412-22.591372 9.34412-8.224271 0-16.415858-3.135923-22.65674-9.407768l-137.60043-138.016718-138.047682 136.576912c-6.239161 6.14455-14.368821 9.247789-22.496761 9.247789-8.255235 0-16.479505-3.168606-22.751351-9.504099-12.416396-12.576374-12.320065-32.800753 0.25631-45.248112l137.887703-136.384249-137.376804-137.824056c-12.480043-12.512727-12.447359-32.768069 0.063647-45.248112 12.512727-12.512727 32.735385-12.447359 45.248112 0.063647l137.567746 137.984034 138.047682-136.575192c12.54369-12.447359 32.831716-12.320065 45.248112 0.25631 12.447359 12.576374 12.320065 32.831716-0.25631 45.248112L557.344443 512.127295 694.752211 649.984034z" fill="#bec0c4" p-id="3806"></path></svg>}
        </div>
        {showCancel && <div className={style.cancel} onClick={cancelSearch}>取消</div>}
      </div>
      <main>
        {showSearchRes ?
          <Result searchResult={searchResult} />
        :
          <Floor floor={floor} />
        }
      </main>
    </div>
  )
}

export default City
