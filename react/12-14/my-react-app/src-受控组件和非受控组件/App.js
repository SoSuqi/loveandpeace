import React, { createRef } from 'react'


// 类组件
class App extends React.Component {
  state = {
    user: '小明'
  }

  submit = () => {
    console.log('受控组件获取姓名', this.state.user)
    console.log('非受控组件获取年龄', this.inp.value)
  }
  changeUser = (e) => {
    this.setState({
      user: e.target.value
    })
  }

  render() {
    return (
      <div>
        {/* 受控组件：受数据控制的组件 */}
        <p>姓名: <input type="text" value={this.state.user} onChange={this.changeUser} /> {this.state.user}</p>
        {/* 非受控组件：通过dom元素获取表单数据 */}
        <p>年龄: <input type="text" defaultValue={100} ref={el => this.inp = el} /></p>
        <button onClick={this.submit}>获取dom</button>
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App