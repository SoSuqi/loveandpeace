import React, { Component } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'
export default class Keyboard extends Component {
  state = {
    curId: '' // 当前高亮按钮的id
  }
  keydown = (e) => {
    // 去所有列表中查找是否存在当前按下的keyCode
    const item = this.props.list.find(v => v.keyCode === e.keyCode)
    if (item) {
      this.props.changeMusic(item)
      this.setState({ curId: item.id })
    }
  }
  keyup = () => {
    this.setState({ curId: '' })
  }

  componentDidMount () {
    document.addEventListener('keydown', this.keydown)
    document.addEventListener('keyup', this.keyup)
  }
  componentWillUnmount () {
    document.removeEventListener('keydown', this.keydown)
    document.removeEventListener('keyup', this.keyup)
  }

  render() {
    const { list, changeMusic, flag } = this.props
    const { curId } = this.state
    return (
      <div className={style.keyboard}>
        {list.map(item =>
          <p
            key={item.id}
            className={classNames({
              [style.active]: curId === item.id, // 高亮
              [style.off]: !flag // 关闭后的高亮
            })}
            onMouseDown={() => {
              this.setState({ curId: item.id })
              changeMusic(item)
            }}
            onMouseUp={this.keyup}
          >{item.keyTrigger}</p>
        )}
      </div>
    )
  }
}
