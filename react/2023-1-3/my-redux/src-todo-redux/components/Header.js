import React, { useRef } from 'react'
import store from '../store'

const Header = () => {

  const inp = useRef()
  const addList = () => {
    store.dispatch({
      type: 'ADD_LIST',
      payload: inp.current.value
    })
    inp.current.value = ''
  }

  return (
    <div className="header">
      <div className="todo-header">
        <input type="text" ref={inp} />
        <button onClick={addList}>add</button>
      </div>
      
      <button onClick={() => {
        store.dispatch({
          type: 'ADD_NUM'
        })
      }}>+</button>
    </div>
  )
}

export default Header