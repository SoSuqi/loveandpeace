import React, { useEffect, useState } from 'react'
import store from '../store'

const Todo = () => {
  const [list, setList] = useState(store.getState().list)

  useEffect(() => {
    // 监听上一次和本次state是否发生改变
    const unsub = store.subscribe(() => {
      setList(store.getState().list)
    })
    return unsub
  }, [])

  const changeDone = (id) => {
    store.dispatch({
      type: 'CHANGE_DONE',
      payload: id
    })
  }

  const remove = (id) => {
    store.dispatch({
      type: 'REMOVE_ITEM',
      payload: id
    })
  }

  return (
    <div className='todo'>
      <ul>
        {list.map(item =>
          <li key={item.id}>
            {item.done ? <s>{item.text}</s> : item.text}
            <button onClick={() => changeDone(item.id)}>完成</button>
            <button onClick={() => remove(item.id)}>删除</button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Todo