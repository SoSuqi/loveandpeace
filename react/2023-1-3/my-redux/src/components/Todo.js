import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'

const Todo = (props) => {
  return (
    <div className='todo'>
      <ul>
        {props.list.map(item =>
          <li key={item.id}>
            {item.done ? <s>{item.text}</s> : item.text}
            <button onClick={() => props.changeDone(item.id)}>完成</button>
            <button onClick={() => props.remove(item.id)}>删除</button>
          </li>
        )}
      </ul>
    </div>
  )
}

const mapState = state => {
  return {
    list: state.list
  }
}
const mapDispatch = dispatch => {
  return {
    remove(id) {
      dispatch({
        type: 'REMOVE_ITEM',
        payload: id
      })
    },
    changeDone: id => {
      dispatch({
        type: 'CHANGE_DONE',
        payload: id
      })
    }
  }
}
export default connect(mapState, mapDispatch)(Todo)