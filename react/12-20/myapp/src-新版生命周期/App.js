import React, { Component } from 'react'
import './app.css'

class Child extends Component {
  state = {
    // 接收父组件的参数作为初始值
    num: this.props.num
  }

  // 新版生命周期，静态方法，无法访问组件实例，state的值在任何情况下都取决于 props时使用(不常用)
  static getDerivedStateFromProps(props, state) {
    return {
      // props更新会改变 state.num
      num: props.num
    }
  }

  render () {
    // console.log(this.state.num, this.props.num)
    console.log(this.a())
    return (
      <div>
        <h2>child</h2>
        {this.state.num}
      </div>
    )
  }
}

class GetError extends Component {
  state = {
    isError: false,
    errorInfo: null
  }
  componentDidCatch (error, info) {
    console.log(error)
    console.log(info)
    this.setState({ isError: true, errorInfo: info })
  }
  render() {
    if (this.state.isError) {
      return (
        <div style={{ background: 'tomato', padding: '20px', borderRadius: '20px' }}>
          <h3>Child组件抛出错误</h3>
          <p>{JSON.stringify(this.state.errorInfo)}</p>
        </div>
      )
    }
    return this.props.children
  }
}

class App extends Component {

  state = {
    num: 10,
    arr: []
  }
  list = React.createRef()
  // render 之后，componentDidUpdate 之前执行，可以获取更新前的信息传给更新后
  getSnapshotBeforeUpdate () {
    // console.log('更新之前子元素总高度', this.list.current.children.length * 40)
    if (this.list.current.children.length * 40 < 400) {
      return null
    }
    return this.list.current.children.length * 40
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    console.log('更新之后', snapshot)
    if (snapshot) {
      this.list.current.scrollTop = snapshot - 400 + 40
    }
  }


  render() {
    const { arr, isError, errorInfo } = this.state
    return (
      <div>
        <h1>生命周期</h1>
        <button onClick={() => {
          this.setState({ num: this.state.num + 1 })
        }}>+</button>
        {this.state.num}

        <hr />

        <GetError>
          <Child num={this.state.num} />
        </GetError>

        
        {/* <button onClick={() => this.setState({ arr: [...arr, arr.length] })}>添加</button>
        <ul ref={this.list}>
          {arr.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul> */}
      </div>
    )
  }
}

export default App
