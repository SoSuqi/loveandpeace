import React, { Component, PureComponent } from 'react'


// PureComponent: 性能优化，PureComponent内部在 shouldComponentUpdate 中浅比较组件中所有的props和state是否有更新
class Child extends PureComponent {

  state = {
    title: 'child',
    arr: new Array(100).fill(0).map((v, i) => i),
  }

  // 性能优化，判断组件中使用的数据是否有更新，如果更新前后的数据没有变化就 return false 减少不必要的更新
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('更新前', this.props, this.state)
  //   console.log('更新后', nextProps, nextState)
  //   if (this.props.num !== nextProps.num || this.state.title !== nextState.title || this.state.arr !== nextState.arr) {
  //     return true
  //   }
  //   return false
  // }
  // 比较所有的属性是否有更新
  // shouldComponentUpdate (nextProps, nextState) {
  //   for (let k in nextProps) {
  //     if (this.props[k] !== nextProps[k]) {
  //       return true
  //     }
  //   }
  //   for (let k in nextState) {
  //     if (this.state[k] !== nextState[k]) {
  //       return true
  //     }
  //   }
  //   return false
  // }
  
  add = () => {
    // 不更新，更新前后的数组指向同一个地址，浅比较时地址一致不会更新
    // this.state.arr.unshift(Math.random())
    // this.setState({
    //   arr: this.state.arr
    // })
    // console.log(this.state.arr)

    // 修改数组或者对象时需要使用新地址覆盖原数据，不能修改原地址
    this.setState({
      arr: [Math.random(), ...this.state.arr]
    })
  }

  render() {
    const { title, arr } = this.state
    console.log('child render')
    return (
      <div>
        <h2>{title} - {this.props.num}</h2>
        <input type="text" value={title} onChange={e => this.setState({ title: e.target.value })} />
        <button onClick={this.add}>添加数据</button>
        <ul>
          {arr.map(item => {
            // console.log('child组件', item)
            return <li key={item}>{item}</li>
          })}
        </ul>
      </div>
    )
  }
}

export default Child
