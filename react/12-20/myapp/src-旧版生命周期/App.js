import React, { Component } from 'react'

class Child extends Component {
  state = {
    num: this.props.num
  }
  // props 更新时才执行，当state依赖于porps时使用 （新版本声明周期已废弃）
  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps')
    this.setState({
      num: nextProps.num
    })
  }
  render () {
    console.log(this.state.num, this.props.num)
    return (
      <div>
        <h2>child</h2>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>+</button>
        {this.state.num}
      </div>
    )
  }
}



class App extends Component {
  // 初始化阶段，初始化state和props
  // constructor(props) {
  //   super(props)
  //   console.log('%c constructor', 'color: red; font-size: 30px')
  // }

  // // 组件挂载之前(新版生命周期已弃用)
  // componentWillMount () {
  //   console.log('%c componentWillMount', 'color: red; font-size: 30px')
  // }

  // // 组件挂载成功(调用接口，操作dom)
  // componentDidMount () {
  //   console.log('%c componentDidMount', 'color: red; font-size: 30px')
  // }

  state = {
    num: 10
  }

  // 注意：不能在更新阶段的生命周期中调用 this.setState
  // 可以判断组件是否应该更新，一般用作性能优化
  // shouldComponentUpdate (nextProps, nextState) {
  //   // nextProps: 最新的props，this.props 当前的props
  //   // nextStat: 最新的state， this.state: 当前的state
  //   // console.log(nextProps, nextState)
  //   console.log('%c shouldComponentUpdate', 'color: red; font-size: 30px')
  //   if (nextState.num <= 10) {
  //     return true
  //   }
  //   return false
  // }
  // 组件更新之前(新版生命周期已弃用)
  // componentWillUpdate () {
  //   console.log('%c componentWillUpdate', 'color: red; font-size: 30px')
  // }
  // // 组件更新之后：可以获取到最新的dom元素
  // componentDidUpdate () {
  //   console.log('%c componentDidUpdate', 'color: red; font-size: 30px')
  // }

  // // 组件销毁之前: (清除异步任务, 定时器、原生事件、请求接口)
  // componentWillUnmount () {
  //   console.log('%c componentWillUnmount', 'color: red; font-size: 30px')
  // }

  render() {
    console.log('%c render', 'color: red; font-size: 30px')
    return (
      <div>
        <h1>生命周期</h1>
        <button onClick={() => {
          this.setState({ num: this.state.num + 1 })
        }}>+</button>
        {this.state.num}
        <hr />
        <Child num={this.state.num} />
      </div>
    )
  }
}

export default App
