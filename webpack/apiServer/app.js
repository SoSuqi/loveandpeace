const express = require('express')

// 创建应用
const app = express()

// app.get('/list', (req, res) => {
// //设置允许跨域访问该接口
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Methods', '*');
//   res.send([1,2,3,4])
// })

app.get('/list', (req, res) => {
  const { resCallback } = req.query
  console.log(resCallback)
  res.setHeader('Content-Type', 'application/javascript;charset=utf-8')
  res.send(`${resCallback}([1,2,3,4,5])`)
})

app.listen(5500, () => {
  console.log('启动成功 http://192.168.0.108:5500')
})
