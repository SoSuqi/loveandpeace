const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/mqq': {
        target: 'http://ustbhuangyi.com',
        pathRewrite: { '^/mqq': '' }
      }
    }
  }
})
