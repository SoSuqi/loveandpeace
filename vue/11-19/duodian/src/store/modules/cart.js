import axios from 'axios'

export default {
  namespaced: true,
  state: () => ({
    list: [], // 所有数据
    cartList: [] // 购物车列表
  }),
  mutations: {
    setList (state, payload) {
      state.list = payload
    },
    addCart (state, payload) {
      // 查找购物车中是否存在此商品
      const index = state.cartList.findIndex(v => v.wareId === payload.wareId)
      if (index > -1) {
        state.cartList[index].wareCount++
      } else {
        state.cartList.push({
          ...payload,
          wareCount: 1,
          wareChecked: true
        })
      }
    },
    changeCartCount (state, { wareId, value }) {
      const index = state.cartList.findIndex(v => v.wareId === wareId)
      state.cartList[index].wareCount = value
      if (value === 0) {
        state.cartList.splice(index, 1)
      }
    },
    changeCartChecked (state, wareId) {
      state.cartList.forEach(item => {
        if (item.wareId === wareId) {
          item.wareChecked = !item.wareChecked
        }
      })
    },
    changeAllChecked (state, checked) {
      state.cartList.forEach(item => {
        item.wareChecked = checked
      })
    }
  },
  getters: {
    total (state) {
      const total = { price: 0, count: 0 }
      state.cartList.forEach(item => {
        if (item.wareChecked) {
          total.price += item.wareCount * item.warePrice
          total.count += item.wareCount
        }
      })
      return total
    }
  },
  actions: {
    async getList (context) {
      const res = await axios({
        url: 'https://searchgw.dmall.com/mp/search/wareSearch',
        method: 'post',
        data: {
          param: '{"venderId":1,"storeId":12527,"businessCode":1,"from":1,"categoryType":1,"pageNum":1,"pageSize":20,"categoryId":"11340","categoryLevel":1}'
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      })
      context.commit('setList', res.data.data.wareList)
    }
  }
}
