export default {
  // 命名空间
  namespaced: true,
  state: state => ({
    num: 10
  }),
  mutations: {
    addNum (state) {
      state.num++
    },
    add (state) {
      console.log('login.js ====> add')
      state.num++
    }
  },
  actions: {},
  getters: {}
}
