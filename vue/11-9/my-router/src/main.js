import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VConsole from 'vconsole'

const vConsole = new VConsole()
console.log(vConsole)
Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

new Vue({
  router, // 给vue实例对象添加路由信息
  render: h => h(App)
}).$mount('#app')
