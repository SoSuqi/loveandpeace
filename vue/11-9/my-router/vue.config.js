const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    // proxy: {
    //   '/mqq': {
    //     target: 'https://m.maizuo.com',
    //     pathRewrite: { '^/mqq': '' },
    //     secure: false,
    //     changeOrigin: true
    //   }
    // }
    setupMiddlewares: (middlewares, devServer) => {
      const movie = require('./data/hot.json')

      devServer.app.get('/hot/movie', (req, res) => {
        const { pagenum, pagesize } = req.query
        if (pagenum && pagesize) {
          res.send({
            status: 0,
            data: {
              films: movie.slice((pagenum - 1) * pagesize, pagenum * pagesize),
              total: movie.length,
              pagenum,
              pagesize
            },
            msg: "ok"
          })
        } else {
          res.send({
            status: -1,
            msg: "参数错误"
          })
        }
      })

      devServer.app.get('/hot/detail', (req, res) => {
        const { filmId } = req.query
        const info = movie.find(v => v.filmId === Number(filmId))
        if (info) {
          res.send({
            status: 0,
            msg: '成功',
            data: info
          })
        } else {
          res.send({
            status: -1,
            msg: '参数错误'
          })
        }
      })

      const city = require('./data/city.json')
      devServer.app.get('/api/city', (req, res) => {
        res.send(city)
      })

      devServer.app.get('/api/login', (req, res) => {
        const { user, pwd } = req.query
        if (user && pwd) {
          res.send({
            status: 0,
            msg: '登陆成功',
            data: {
              token: user + Math.random() + pwd
            }
          })
        } else {
          res.send({
            status: -1,
            msg: '用户名或者密码错误'
          })
        }
      })

      return middlewares;
    },
  }
})
