const BASE_URL = 'https://api-hmugo-web.itheima.net/api/public/v1'

const request = function ({ url, method = 'get', data = {} }) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: BASE_URL + url,
      method: method.toUpperCase(),
      data,
      success: res => {
        resolve(res.data)
      },
      fail: (e) => {
        reject(e)
      }
    })
  })
}
request.get = function (url, data) {
  return request({ url, data })
}
request.post = function (url, data) {
  return request({ url, method: 'post', data })
}
export default request