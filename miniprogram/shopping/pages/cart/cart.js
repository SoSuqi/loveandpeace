// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartlist: [],
    total: {
      count: 0,
      price: 0
    },
    checkAll: true
  },
  changeCount (e) {
    const { num, index } = e.currentTarget.dataset
    if (num === 0) {
      wx.showModal({
        title: '提示',
        content: '确定要删除此商品吗？',
        success: res => {
          if (res.confirm) {
            const cartlist = [...this.data.cartlist]
            cartlist.splice(index, 1)
            this.setData({ cartlist })
            this.getTotal()
          }
        }
      })
    } else {
      const key = `cartlist[${index}].count`
      this.setData({
        [key]: num
      })
      this.getTotal()
    }
  },
  getTotal () {
    this.setData({
      total: this.data.cartlist.reduce((prev, val) => {
        return {
          count: prev.count + (val.checked ? val.count : 0),
          price: prev.price + (val.checked ? val.count * val.goods_price : 0),
        }
      }, { count: 0, price: 0 })
    })
    wx.setStorageSync('cartlist', this.data.cartlist)
  },
  changeCart (e) {
    console.log(e.detail.value); // 获取选中的下标
    const cartlist = [...this.data.cartlist]
    cartlist.forEach((item, index) => {
      item.checked = e.detail.value.includes(index + '')
    })
    this.setData({
      checkAll: e.detail.value.length === this.data.cartlist.length,
      cartlist
    })
    this.getTotal()
  },
  changeAll (e) {
    this.setData({
      cartlist: this.data.cartlist.map(v => {
        v.checked = this.data.checkAll
        return v
      })
    })
    this.getTotal()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const cartlist = wx.getStorageSync('cartlist') || []
    this.setData({
      cartlist,
      checkAll: cartlist.every(v => v.checked)
    })
    this.getTotal()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})