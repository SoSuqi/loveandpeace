// components/el-tab-pane/el-tab-pane.js
Component({
  relations: {
    '../el-tabs/el-tabs': {
      type: 'parent', // 关联的目标节点应为父节点
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    label: {
      type: String
    },
    name: {
      type: String
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
