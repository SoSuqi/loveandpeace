

const request = function ({ url, method = 'get' }) {
  return new Promise((resolve, reject) => {
    wx.request({
      url,
      method: method.toUpperCase(),
      success (res) {
        resolve(res)
      },
      fail (err) {
        reject(err)
      }
    })
  })
}
request.get = function (url) {
  return request({ url, method: 'get' })
}
request.post = function (url) {
  return request({ url, method: 'post' })
}

export default request